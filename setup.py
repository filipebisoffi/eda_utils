from setuptools import setup

setup(name='eda_utils',
      version='0.1',
      description='Tools to make EDA easier',
      author='Filipe Bonin',
      author_email='filipe.bisoffi@meupag.com.br',
      license='MIT',
      packages=['ExploratoryAnalyser'],
      zip_safe=False)