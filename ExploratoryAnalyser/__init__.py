from statsmodels.distributions.empirical_distribution import ECDF
from scipy import stats
import matplotlib.pyplot as plt  
import seaborn as sns 
import pandas as pd


class ExploratoryAnalyser:
   
    def __init__(self, dataSet, targetVariable = 'target', safraVariable = 'safra'):
        """ Initiator for ExploratoryAnalyser class
        """
        self.targetVariable = targetVariable
        self.dataSet = dataSet
        self.safraVariable = safraVariable
        
        try:
            dataSet[targetVariable]
        except:
            raise ValueError('Target variable "%s" not found on data set.' % targetVariable)
        self.targetDomain = self.getTargetDomain()
  
        try:
            dataSet[safraVariable]
        except: 
            print('Safra variable "%s" not found on data set.' % safraVariable)

        self.dataSummary = self.summary()
            

    def getTargetDomain(self):
        """Compute unique values for target variable. Currently it must be a binary variable."""
        targetVariable = self.targetVariable
        uniqueValue = self.dataSet[targetVariable].unique()
        if uniqueValue.shape[0] != 2: 
            raise ValueError('Target variable "%s" is not binary.' % targetVariable)
        else:
            return(uniqueValue)
    
    def checkVariableType(self,explanatoryVariable):
        """ Return variable type mapped according to the type of plot to be done. Currently:
        continous = ['float64','int64']
        categorical = ['object']
        If variable type is not yet mapped a error will be raised.
        """
        continous = ['float64','int64']
        categorical = ['object']
        if (self.dataSet[explanatoryVariable].dtype in continous): 
            return "continous"
        elif (self.dataSet[explanatoryVariable].dtype in categorical): 
            return "categorical"
        else:
            raise ValueError("Not ready to deal with '%s' data type." % self.dataSet[explanatoryVariable].dtype)

    
    def plotContinousVariable(self,dataSet,explanatoryVariable,targetVariable,domain,fontSize):
        # Density plots to compare conditional distribution
        plt.style.use('fivethirtyeight')
        fig, ax = plt.subplots(figsize=(20,8), nrows=1, ncols=2)
        for i in range(domain.shape[0]):
            subset = dataSet[dataSet[targetVariable] == domain[i]]
            try:
                sns.distplot(subset[explanatoryVariable],ax=ax[0],hist=False,kde=True,
                         kde_kws={'shade': True, 'linewidth': 2},label=domain[i])
            except: 
                tempBw = dataSet[explanatoryVariable].std()/10
                sns.distplot(subset[explanatoryVariable],ax=ax[0],hist=False,kde=True,
                         kde_kws={'shade': True, 'linewidth': 2,'bw': tempBw},label=domain[i])
        ax[0].set_title('Histogram - n = {}'.format(len(dataSet)))

        # Empirical CDF - KS
        for i in range(domain.shape[0]):
            subset = dataSet[dataSet[targetVariable] == domain[i]]
            cdf = ECDF(subset[explanatoryVariable])
            ax[1].plot(cdf.x, cdf.y, label=domain[i])
        ax[1].set_xlabel(explanatoryVariable)
        ax[1].legend(loc='upper left')
        ks = stats.ks_2samp(dataSet.loc[dataSet[targetVariable] == domain[0], explanatoryVariable],
                      dataSet.loc[dataSet[targetVariable] == domain[1], explanatoryVariable])[0]
        ax[1].set_title('CDF - KS = {}'.format(round(ks,2)))                
        plt.show()
        
    def plotCategoricalVariable(self,dataSet,explanatoryVariable,targetVariable,domain,fontSize,safraVariable):
        # Bar chart showing distribution of target variables across categories
        plt.style.use('fivethirtyeight')
        fig, ax = plt.subplots(figsize=(20,8), nrows=1, ncols=2)

        dataSet.groupby(explanatoryVariable)[targetVariable].\
            value_counts(normalize = True , dropna = False).unstack(1).reset_index().\
            plot(x = explanatoryVariable, y = domain,kind = 'bar',ax = ax[0])
        ax[0].set_title(explanatoryVariable,fontsize = fontSize*2)
        plt.setp(ax[0].get_xticklabels(), rotation=70, horizontalalignment='right')
        ax[0].legend(bbox_to_anchor=(1.05, 1), loc='upper right', borderaxespad=0.)
        ax[0].set_xlabel(explanatoryVariable)
        ax[0].set_ylabel('Percentage')

        for i in ax[0].patches:
             ax[0].text(i.get_x(), i.get_height()+0.01, \
             str(round((i.get_height()*100), 2)), fontsize=fontSize, color='black')
        
        # Line chart over period to analyse stability of distribution
        groupedDF = dataSet.groupby([safraVariable,explanatoryVariable])[targetVariable].\
            value_counts(normalize = True , dropna = False).unstack(1).reset_index()
            
        groupedDF = groupedDF[groupedDF[targetVariable] == domain[0]]    
        groupedDF.drop(targetVariable,axis=1).plot(x= safraVariable,ax = ax[1])
        ax[1].ticklabel_format(useOffset=False, style='plain', axis='y')

        ax[1].set_ylabel("Percentage")
        ax[1].set_title("Percentage for  %s = '%s'" %  (targetVariable,domain[0]))
        plt.show()
    
    def plotDistributions(self,explanatoryVariable,maxCategories = 10):
        """ Main function to plot analyses
         Basically just determine whether to call categorical or continoues plots 
         explanatoryVariable = String, variable that you want to analyse
         maxCategories = Int, default 10, variable that you use to limit categories on plot """
        fontSize = 10
        targetVariable = self.targetVariable
        targetDomain = self.targetDomain
        safraVariable = self.safraVariable
        try:
            self.dataSet[explanatoryVariable]
        except:
            raise ValueError('Explanatory Variable "%s" not found on data set.' % explanatoryVariable)

        variableType = self.checkVariableType(explanatoryVariable)
    
        print("Chart for '%s':" % explanatoryVariable)

        if ( variableType == "continous"):
            self.plotContinousVariable(self.dataSet,explanatoryVariable,targetVariable,targetDomain,fontSize)

        elif (variableType == "categorical"):
            if self.dataSummary.at[explanatoryVariable,'Unique domain'] <= maxCategories:
                self.plotCategoricalVariable(self.dataSet,explanatoryVariable,targetVariable,targetDomain,fontSize,safraVariable)
            else:
                print("Too many categories!")

        else: 
            self.checkVariableType(explanatoryVariable)
        
    def checkMissing(self):
        return self.dataSet.isna().mean().sort_values(ascending=False)
    
    def checkDomain(self):
        uniqueCountSeries = pd.Series([])
        for i in self.dataSet.columns:
            element = pd.Series(self.dataSet[i].nunique(),index = [i])
            uniqueCountSeries = uniqueCountSeries.append(element)

        return uniqueCountSeries.sort_values(ascending=False)
    
    def checkMode(self):
        modeSeries  = pd.Series([])
        modePercentageSeries = pd.Series([])
        for i in self.dataSet.columns:
            mode = self.dataSet[i].value_counts(normalize=True).sort_values(ascending=False)
            modeSeries = modeSeries.append(pd.Series(mode.index[0],index = [i]) )
            modePercentageSeries = modePercentageSeries.append(pd.Series(mode.iloc[0],index = [i]))
        return modeSeries, modePercentageSeries

    
    def summary(self):
        missing = pd.DataFrame(self.checkMissing(),columns=["Missing"])
        domain = pd.DataFrame(self.checkDomain(),columns=["Unique domain"])
        dataTypes = pd.DataFrame(self.dataSet.dtypes,columns=["Type"])
        modeSeries, modePercentageSeries = self.checkMode()
        modeSeries = pd.DataFrame(modeSeries,columns = ["Mode"])
        modePercentageSeries = pd.DataFrame(modePercentageSeries,columns = ["Mode percentage"])
        return missing.join(domain).join(dataTypes).join(modeSeries).join(modePercentageSeries)
        